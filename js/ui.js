function uiPreload(){
  var sprites = ["fullscreen","button"];
  for(s of sprites){
    scene.load.spritesheet(s, '/ui/'+s+'.png', { frameWidth: 64, frameHeight: 64 });
  }
  hud = scene.add.group();
}

function btn_fullscreen(){
  var button = hud.create(width-16, 16, 'fullscreen', 0).setOrigin(1, 0).setInteractive();
  button.alpha = .5;
  button.setScrollFactor(0);

  button.on('pointerup', function () {
    if (scene.scale.isFullscreen){
      button.setFrame(0);
      scene.scale.stopFullscreen();
    }else{
      button.setFrame(1);
      scene.scale.startFullscreen();
    }
  });

  const btn_fs = button;
  return button;
}

function btn_create(func){
  var button = hud.create(width-16, game.config.height * .85, 'button', 0).setOrigin(1, 0).setInteractive();
  button.alpha = .5;
  button.setScrollFactor(0);
  button.on('pointerdown', function(a){
    button.pointerdown(); 
    button.setFrame(1);
  });

  button.on('pointerup',function(){
    button.pointerup(); 
    button.setFrame(0);
  });
  return button;
}


function loadScreen(msg){
  var loadingText = scene.make.text({
    x: game.config.width / 2,
    y: game.config.height / 2 - 50,
    text: msg,
    style: {
      font: '20px monospace',
      fill: '#ffffff'
    }
  });
  loadingText.setOrigin(0.5, 0.5);
  scene.load.on('complete', function () {
    loadingText.destroy();
  });
}
