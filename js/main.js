const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 960,
  height: 540,
  scene: {
    preload: preload,
    update: update,
    create: create
  },
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'phaser-example',
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: 960,
    height: 540
  },
  physics: {
    default: 'arcade',
      arcade: {
        gravity: {
          y: 300
        },
      }
  }
};


const game = new Phaser.Game(config);
let scene;
let width = config.width;
let height = config.height;

function preload (){
  scene = this;
  this.load.path = 'assets';
  //Load images
  var images=['bg.png'];
  for(var i=0;i<images.length;i++){
    var name = images[i].split(".")[0];
    this.load.image(name, '/images/'+images[i]);
  }

  //Load Sprites
  var sprites=['sprite_1.png'];
  for(var i=0;i<sprites.length;i++){
    var name = sprites[i].split(".")[0];
    this.load.image(name, '/sprites/'+sprites[i]);
  }

  //run preload function for UI elements
  uiPreload();
}

function create (){
  bg = this.add.image(0, 0, 'bg').setOrigin(0).setDisplaySize(game.config.width, game.config.height);
  sprite_1 = this.add.sprite(game.config.width / 2, game.config.height / 2, 'sprite_1');
  sprite_1.setScale(0.5);
  sprite_1.setInteractive();
  sprite_1.on('pointerdown', function (pointer) {
    console.log(this);
    game.canvas.requestFullscreen();
  });

  //load fullscreen button
  btn_fullscreen();

  //add touch screen buttons
  button1 = btn_create();
  button1.pointerdown = function(){console.log("clicked")};
  button1.pointerup = function(){console.log("pointer up")};
}

function update(){

}

